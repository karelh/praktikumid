package kodutoo;

import lib.TextIO;

public class Level {
	
	public static void m2ng(int punktid){
		String[][] laud = {
				{ "-", "-", "-", "-", "-", "-", "-", "-", "-", "-", "-", "-", "-", "-", "-", "-", "-", "-", "-", "-" },
				{ "-", "-", "-", "-", "-", "-", "-", "-", "-", "-", "-", "-", "-", "-", "-", "-", "-", "-", "-", "-" },
				{ "|", "|", "x", "|", "1", "o", "o", "o", "|", "o", "o", "2", "|", "o", "|", "o", "|", "3", "|", "|" },
				{ "|", "|", "o", "-", "-", "o", "|", "o", "o", "o", "-", "-", "-", "o", "|", "o", "|", "o", "|", "|" },			
				{ "|", "|", "o", "o", "o", "o", "|", "o", "o", "o", "o", "o", "o", "o", "o", "o", "|", "o", "|", "|" },
				{ "|", "|", "o", "o", "o", "o", "|", "o", "|", "o", "o", "o", "o", "o", "o", "o", "|", "o", "|", "|" },
				{ "|", "|", "-", "-", "-", "o", "o", "o", "|", "o", "-", "-", "-", "-", "o", "1", "|", "o", "|", "|" },
				{ "|", "|", "3", "o", "|", "o", "o", "o", "|", "o", "o", "2", "o", "o", "o", "-", "-", "o", "|", "|" },
				{ "|", "|", "-", "o", "|", "o", "-", "o", "|", "o", "o", "-", "-", "o", "o", "o", "o", "o", "|", "|" },
				{ "|", "|", "o", "o", "|", "1", "o", "o", "o", "o", "o", "o", "o", "o", "o", "o", "o", "-", "|", "|" },
				{ "|", "|", "o", "o", "-", "-", "-", "o", "o", "-", "-", "-", "-", "o", "o", "|", "o", "o", "|", "|" },
				{ "|", "|", "o", "o", "o", "o", "o", "o", "o", "o", "3", "o", "o", "o", "o", "|", "o", "2", "|", "|" },
				{ "-", "-", "-", "-", "-", "-", "-", "-", "-", "-", "-", "-", "-", "-", "-", "-", "-", "-", "-", "-" },
				{ "-", "-", "-", "-", "-", "-", "-", "-", "-", "-", "-", "-", "-", "-", "-", "-", "-", "-", "-", "-" },
				
		};
		tryki(laud, 2, 2, punktid);

	}

	public static void tryki(String[] Numbrid) {
		System.out.println();
		for (int i = 0; i < Numbrid.length; i++) {
			System.out.print(Numbrid[i] + " ");
		}

	}

	public static void tryki(String[][] maatriks, int x, int y, int punktid) {
		
		//V�ljaku tr�kkimine
		
		for (int i = 0; i < maatriks.length; i++) {
			tryki(maatriks[i]);

		}

		liigu(maatriks, x, y, punktid);

	}

	public static void liigu(String[][] maatriks, int x, int y, int punktid) {
		
		//Liikumine ja m�ngu kaotamine kui minna vastu seinu.
		
		String suund = TextIO.getln();
		l6pp(maatriks, punktid);
		if (x < 2 || y < 2 || x > 11 || y > 17) {

			System.out.println("L�ksid piiridest v�lja!");
			l2bi(punktid);
		}

		else if ((x == 2 && y == 3) || (x == 3 && y == 3) || (x == 3 && y == 4)) {

			System.out.println("L�ksid vastu 1. seina!");
			l2bi(punktid);
		}
		else if ((x == 6 && y == 2) || (x == 6 && y == 3) || (x == 6 && y == 4) || (x == 7 && y == 4)
				|| (x == 8 && y == 4) || (x == 9 && y == 4) || (x == 10 && y == 4) || (x == 10 && y == 5)
				|| (x == 10 && y == 6)) {
			System.out.println("L�ksid vastu 2. seina!");
			l2bi(punktid);

		} 
		else if (x == 8 && y == 2) {
			System.out.println("L�ksid vastu 3. seina!");
			l2bi(punktid);
		}
		else if ((x == 3 && y == 6) || (x == 4 && y == 6) || (x == 5 && y == 6)) {
			System.out.println("L�ksid vastu 4. seina!");
			l2bi(punktid);
		}
		else if (x == 8 && y == 6) {
			System.out.println("L�ksid vastu 5. seina!");
			l2bi(punktid);
		} 
		else if (x == 2 && y == 8) {
			System.out.println("L�ksid vastu 6. seina!");
			l2bi(punktid);
		}
		else if ((x == 5 && y == 8) || (x == 6 && y == 8) || (x == 7 && y == 8) || (x == 8 && y == 8)) {
			System.out.println("L�ksid vastu 7. seina!");
			l2bi(punktid);
		} 
		else if ((x == 3 && y == 10) || (x == 3 && y == 11) || (x == 3 && y == 12) || (x == 2 && y == 12)) {
			System.out.println("L�ksid vastu 8. seina!");
			l2bi(punktid);
		}
		else if ((x == 6 && y == 10) || (x == 6 && y == 11) || (x == 6 && y == 12) || (x == 6 && y == 13)) {
			System.out.println("L�ksid vastu 9. seina!");
			l2bi(punktid);
		} 
		else if ((x == 8 && y == 11) || (x == 8 && y == 12)) {
			System.out.println("L�ksid vastu 10. seina!");
			l2bi(punktid);
		}
		else if ((x == 10 && y == 9) || (x == 10 && y == 10) || (x == 10 && y == 11) || (x == 10 && y == 12)) {
			System.out.println("L�ksid vastu 11. seina!");
			l2bi(punktid);
		} 
		else if ((x == 2 && y == 14) || (x == 3 && y == 14)) {
			System.out.println("L�ksid vastu 12. seina!");
			l2bi(punktid);
		} 
		else if ((x == 2 && y == 16) || (x == 3 && y == 16) || (x == 4 && y == 16) || (x == 5 && y == 16)
				|| (x == 6 && y == 16) || (x == 7 && y == 16) || (x == 7 && y == 15)) {
			System.out.println("L�ksid vastu 13. seina!");
			l2bi(punktid);
		} 
		else if (x == 9 && y == 17) {
			System.out.println("L�ksid vastu 14. seina!");
			l2bi(punktid);
		} 
		else if ((x == 10 && y == 15) || (x == 11 && y == 15)) {
			System.out.println("L�ksid vastu 15. seina!");
			l2bi(punktid);
		}

		else if (suund.equals("a")) {
			if (maatriks[x][y - 1] == "o" && suund.equals("a")) {
				maatriks[x][y] = " ";
				maatriks[x][y = y - 1] = "x";
				punktid = punktid + 100;
			} 
			else if (maatriks[x][y - 1] == "1" && suund.equals("a")) {
				maatriks[x][y] = " ";
				maatriks[x][y = y - 1] = "x";
				punktid = punktid + 1000;
			} 
			else if (maatriks[x][y - 1] == "2" && suund.equals("a")) {
				maatriks[x][y] = " ";
				maatriks[x][y = y - 1] = "x";
				punktid = punktid + 2000;
			}
			else if (maatriks[x][y - 1] == "3" && suund.equals("a")) {
				maatriks[x][y] = " ";
				maatriks[x][y = y - 1] = "x";
				punktid = punktid + 3000;
			} 
			else {
				maatriks[x][y] = " ";
				maatriks[x][y = y - 1] = "x";
			}

			System.out.println("Punkte:" + punktid);

			tryki(maatriks, x, y, punktid);

		}
		else if (suund.equals("d")) {
			if (maatriks[x][y + 1] == "o" && suund.equals("d")) {
				punktid = punktid + 100;
				maatriks[x][y] = " ";
				maatriks[x][y = y + 1] = "x";
			} 
			else if (maatriks[x][y + 1] == "1" && suund.equals("d")) {
				maatriks[x][y] = " ";
				maatriks[x][y = y + 1] = "x";
				punktid = punktid + 1000;
			} 
			else if (maatriks[x][y + 1] == "2" && suund.equals("d")) {
				maatriks[x][y] = " ";
				maatriks[x][y = y + 1] = "x";
				punktid = punktid + 2000;
			}
			else if (maatriks[x][y + 1] == "3" && suund.equals("d")) {
				maatriks[x][y] = " ";
				maatriks[x][y = y + 1] = "x";
				punktid = punktid + 3000;
			} 
			else {
				maatriks[x][y] = " ";
				maatriks[x][y = y + 1] = "x";
			}
			System.out.println("Punkte:" + punktid);

			tryki(maatriks, x, y, punktid);
		} 
		else if (suund.equals("s")) {
			if (maatriks[x + 1][y] == "o" && suund.equals("s")) {
				punktid = punktid + 100;
				maatriks[x][y] = " ";
				maatriks[x = x + 1][y] = "x";
			} 
			else if (maatriks[x + 1][y] == "1" && suund.equals("s")) {
				maatriks[x][y] = " ";
				maatriks[x = x + 1][y] = "x";
				punktid = punktid + 1000;
			} 
			else if (maatriks[x + 1][y] == "2" && suund.equals("s")) {
				maatriks[x][y] = " ";
				maatriks[x = x + 1][y] = "x";
				punktid = punktid + 2000;
			}
			else if (maatriks[x + 1][y] == "3" && suund.equals("s")) {
				maatriks[x][y] = " ";
				maatriks[x = x + 1][y] = "x";
				punktid = punktid + 3000;
			} 
			else {
				maatriks[x][y] = " ";
				maatriks[x = x + 1][y] = "x";
			}
			System.out.println("Punkte:" + punktid);
			tryki(maatriks, x, y, punktid);
		} 
		else if (suund.equals("w")) {
			if (maatriks[x - 1][y] == "o" && suund.equals("w")) {
				punktid = punktid + 100;
				maatriks[x][y] = " ";
				maatriks[x = x - 1][y] = "x";
			}
			else if (maatriks[x - 1][y] == "1" && suund.equals("w")) {
				maatriks[x][y] = " ";
				maatriks[x = x - 1][y] = "x";
				punktid = punktid + 1000;
			} 
			else if (maatriks[x - 1][y] == "2" && suund.equals("w")) {
				maatriks[x][y] = " ";
				maatriks[x = x - 1][y] = "x";
				punktid = punktid + 2000;
			}
			else if (maatriks[x - 1][y] == "3" && suund.equals("w")) {
				maatriks[x][y] = " ";
				maatriks[x = x - 1][y] = "x";
				punktid = punktid + 3000;
			}  
			else {
				maatriks[x][y] = " ";
				maatriks[x = x - 1][y] = "x";
			}
			System.out.println("Punkte:" + punktid);

			tryki(maatriks, x, y, punktid);
		}
		else {
			System.out.println("Sisestage w, a, s v�i d");
			liigu(maatriks, x, y, punktid);
			tryki(maatriks, x, y, punktid);
		}

	}

	public static void l2bi(int punktid) {
		
		//M�ng saab l�bi. Uus m�ng?
		
		int skoor = (int) (punktid / elapsedTime() * 10);
		System.out.println("M�ng l�bi!!!");
		System.out.println("Sinu aeg oli: " + elapsedTime() + " sekundit");
		System.out.println("Punkte said: " + punktid);
		System.out.println("Sinu skoor on: " + skoor);
		System.out.println();
		System.out.println("Kas tahad uuesti m�ngida?(y=jah)");
		String uuesti = TextIO.getln();
		if (uuesti.equals("y")) {
			start = System.currentTimeMillis();
			m2ng(0);
		}
		else {
			while (uuesti != ("y")) {
				System.out.println("Kas tahad uuesti m�ngida?(y=jah)");
				uuesti = TextIO.getln();
				if (uuesti.equals("y")) {
					start = System.currentTimeMillis();
					m2ng(0);
				}
			}
		}
	}

	public static void l6pp(String[][] maatriks, int punktid) {
		
		//M�ng saab l�bi kui k�ik punktid on s��dud ja saab boonus punkte selle eest.
		
		if (maatriks[2][2] != "o" && maatriks[3][2] != "o" && maatriks[4][2] != "o" && maatriks[5][2] != "o"
				&& maatriks[7][2] != "3" && maatriks[9][2] != "o" && maatriks[10][2] != "o" && maatriks[11][2] != "o"
				&& maatriks[4][3] != "o" && maatriks[5][3] != "o" && maatriks[7][3] != "o" && maatriks[8][3] != "o"
				&& maatriks[9][3] != "o" && maatriks[10][3] != "o" && maatriks[11][3] != "o" && maatriks[2][4] != "1"
				&& maatriks[4][4] != "o" && maatriks[5][4] != "o" && maatriks[11][4] != "o" && maatriks[2][5] != "o"
				&& maatriks[3][5] != "o" && maatriks[4][5] != "o" && maatriks[5][5] != "o" && maatriks[6][5] != "o"
				&& maatriks[7][5] != "o" && maatriks[8][5] != "o" && maatriks[9][5] != "1" && maatriks[11][5] != "o"
				&& maatriks[2][6] != "o" && maatriks[6][6] != "o" && maatriks[7][6] != "o" && maatriks[9][6] != "o"
				&& maatriks[11][6] != "o" && maatriks[2][7] != "o" && maatriks[3][7] != "o" && maatriks[4][7] != "o"
				&& maatriks[5][7] != "o" && maatriks[6][7] != "o" && maatriks[7][7] != "o" && maatriks[8][7] != "o"
				&& maatriks[9][7] != "o" && maatriks[10][7] != "o" && maatriks[11][7] != "o" && maatriks[3][8] != "o"
				&& maatriks[4][8] != "o" && maatriks[9][8] != "o" && maatriks[10][8] != "o" && maatriks[11][8] != "o"
				&& maatriks[2][9] != "o" && maatriks[3][9] != "o" && maatriks[4][9] != "o" && maatriks[5][9] != "o"
				&& maatriks[6][9] != "o" && maatriks[7][9] != "o" && maatriks[8][9] != "o" && maatriks[9][9] != "o"
				&& maatriks[11][9] != "o" && maatriks[2][10] != "o" && maatriks[4][10] != "o" && maatriks[5][10] != "o"
				&& maatriks[7][10] != "o" && maatriks[8][10] != "o" && maatriks[9][10] != "o" && maatriks[11][10] != "o"
				&& maatriks[2][11] != "2" && maatriks[4][11] != "o" && maatriks[5][11] != "o" && maatriks[7][11] != "2"
				&& maatriks[9][11] != "o" && maatriks[11][11] != "o" && maatriks[4][12] != "o" && maatriks[5][12] != "o"
				&& maatriks[7][12] != "o" && maatriks[9][12] != "o" && maatriks[11][12] != "o" && maatriks[2][13] != "o"
				&& maatriks[3][13] != "o" && maatriks[4][13] != "o" && maatriks[5][13] != "o" && maatriks[7][13] != "o"
				&& maatriks[8][13] != "o" && maatriks[9][13] != "o" && maatriks[10][13] != "o"
				&& maatriks[11][13] != "3" && maatriks[4][14] != "o" && maatriks[5][14] != "o" && maatriks[6][14] != "o"
				&& maatriks[7][14] != "o" && maatriks[8][14] != "o" && maatriks[9][14] != "o" && maatriks[10][14] != "o"
				&& maatriks[11][14] != "o" && maatriks[2][15] != "o" && maatriks[3][15] != "o" && maatriks[4][15] != "o"
				&& maatriks[5][15] != "o" && maatriks[6][15] != "1" && maatriks[8][15] != "o" && maatriks[9][15] != "o"
				&& maatriks[8][16] != "o" && maatriks[9][16] != "o" && maatriks[10][16] != "o"
				&& maatriks[11][16] != "o" && maatriks[2][17] != "3" && maatriks[3][17] != "o" && maatriks[4][17] != "o"
				&& maatriks[5][17] != "o" && maatriks[6][17] != "o" && maatriks[7][17] != "o" && maatriks[8][17] != "o"
				&& maatriks[10][17] != "o" && maatriks[11][17] != "2") {
			System.out.println("Tubli, k�ik punktid kogutud. Selle eest saad + 50 000 punkti");
			punktid = punktid +50000;
			l2bi(punktid);

		}

	}

	public static long start;
		//Aeg tiksuma.
	public static double elapsedTime() {
		long now = System.currentTimeMillis();
		return (now - start) / 1000.0;
		// @author Robert Sedgewick
		// @author Kevin Wayne
	}

}
