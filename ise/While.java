package ise;

import java.awt.event.KeyEvent;

public class While {

	public static void main(String[] args) {
		int number;   // The number to be printed.
		number = 1;   // Start with 1.
		while ( number < 6 ) {  // Keep going as long as number is < 6.
		    System.out.println(number);
		    number++;  // Go on to the next number. number = number +1
		}
		System.out.println("Done!");
		
	}
	 public void keyTyped(KeyEvent e) {

	        if (e.getKeyCode() == KeyEvent.VK_RIGHT ) {
	            System.out.println("Right typed.");
	        } 
	        else if (e.getKeyCode() == KeyEvent.VK_LEFT ) {
	            System.out.println("Left typed.");
	        } 
	        else if (e.getKeyCode() == KeyEvent.VK_UP ) {
	            System.out.println("Up typed.");
	        } 
	        else if (e.getKeyCode() == KeyEvent.VK_DOWN ) {
	            System.out.println("Down typed.");
	        }
	        else{
	            System.out.println("Key typed: " + e.getKeyChar());
	        }
	    }

}
