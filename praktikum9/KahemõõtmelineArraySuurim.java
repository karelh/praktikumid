package praktikum9;

public class KahemõõtmelineArraySuurim {

	public static void main(String[] args) {
		int[] massiiv = {1, 19, 6, 7, 8, 3, 5, 7, 21, 3};
		int suurimm = 0;
		for (int i = 0; i < massiiv.length; i++) {
			if (massiiv[i] > suurimm){
				suurimm = massiiv[i];
			}
		}
		System.out.println("Ühemõõtmelise suurim: " + suurimm);
		kahemõõtmeline();
	}
	public static void kahemõõtmeline() {
		int suurimm = 0;
		int[][] neo = {
				{1, 3, 6, 7},
				{2, 3, 3, 1},
				{17, 4, 5, 0},
				{-20, 13, 16, 17}
		};
		for (int i = 0; i < neo.length; i++) {
			for (int j = 0; j < neo.length; j++) {
				if (neo[i][j] > suurimm){
					suurimm = neo[i][j];
				}
			}
		}		
		
		System.out.println("Kahemõõmelise suurim: " + suurimm);	
	}
}