package praktikum6;

import lib.TextIO;

public class ReverseWord {
	
	public static void main(String[] args){
		System.out.println("Sisesta oma s�na mida tahad n�ga tagurpidi: ");
		String x = TextIO.getln();
		System.out.println(tagurpidi(x));
		
	}
	public static String tagurpidi(String s�na) {
		String reverse = "";
        
        int length = s�na.length();
        for (int i = length - 1; i >= 0; i--) {
            reverse = reverse + s�na.charAt(i); 
        }
        return reverse;
    }
}