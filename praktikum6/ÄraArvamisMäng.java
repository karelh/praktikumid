package praktikum6;

import lib.TextIO;

public class �raArvamisM�ng {

	public static void main(String[] args) {
		
		m�ng();
	}

	public static void m�ng(){
		
		System.out.println("Arvuti m�tleb numbrile 1-100ni. Arva �ra mis see on: ");
		int x = TextIO.getlnInt();
		int y = (int)(Math.random()*100+1);
		
		while(x != y){
			if(x < y){
				System.out.println("suurem");
				System.out.println("proovi uuesti: ");
				x = TextIO.getlnInt();
			}
			if (x > y){
				System.out.println("v�iksem.");
				System.out.println("proovi uuesti: ");
				x = TextIO.getlnInt();
			}
			if (x == y){
				System.out.println("Arvasidki �ra! Vastus oli: " + y);
			}
		}	
	}
}
