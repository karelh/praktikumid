package praktikum2;


public class NimePikkus extends Thread 
{
	public static void main(String[] args) throws InterruptedException
    {
        NimePikkus s = new NimePikkus();

        s.startThread();

        while (true)
        {
            int[] curTime = s.getTime();
            System.out.println(curTime[1] + " : " + curTime[2]);
        }

    }
    private long startTime;
    private boolean started;

    public void startThread()
    {
        this.startTime = System.currentTimeMillis();
        this.started = true;
        this.start();
    }

    public void run()
    {
        while (started)
        {
            // empty code since currentTimeMillis increases by itself
        }
    }


    public int[] getTime()
    {
        long milliTime = System.currentTimeMillis() - this.startTime;
        int[] out = new int[]{0, 0, 0, 0};
      
        out[1] = (int)(milliTime / 60000        ) % 60;
        out[2] = (int)(milliTime / 1000         ) % 60;
        

        return out;
    }

    public void stopThread()
    {
        this.started = false;
    }
}
