package praktikum2;

import lib.TextIO;

public class TrykiEkraanile { 
	public static void main(String[] args){
		
		start = System.currentTimeMillis();
		System.out.println("s");
		String u =TextIO.getInputFileName();
		System.out.println(u);
		
		System.out.println(elapsedTime());
	}

    private static long start;

    /**
     * Initializes a new stopwatch.
     */
    public TrykiEkraanile() {
    } 


    /**
     * Returns the elapsed CPU time (in seconds) since the stopwatch was created.
     *
     * @return elapsed CPU time (in seconds) since the stopwatch was created
     */
    public static double elapsedTime() {
        long now = System.currentTimeMillis();
        return (now - start) / 1000.0;
    }
    public static void oota(int x) {

		try {
			Thread.sleep(x); // 1000 milliseconds is one second.
		} catch (InterruptedException ex) {
			Thread.currentThread().interrupt();
		}

	}
} 

