package praktikum10;

public class Fibo {

	public static void main(String[] args) {
		int x=0;
		int y=0;
		
		while (x>-1){
			x= x+1;
			y=y+1;
			System.out.println(y + ". " + leiaFib(x));
		}

	}

	public static int leiaFib(int n) {

		if (n == 0) {
			return 0;
		}
		if (n == 1) {
			return 1;
		}
		
		return (leiaFib(n - 1) + leiaFib(n - 2));
	}
}
