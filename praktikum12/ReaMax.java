package praktikum12;

public class ReaMax {
	public static void main(String[] args){
		int [][] neo = {
				{1, 1, 1, 1, 1},
				{2, 3, 4, 5, 6},
				{3, 4, 5, 6, 7},
				{4, 5, 6, 7, 8},
				{5, 6, 7, 8, 9},
 };
		Tryki(ridadeMaksimumid(neo));
		
	}
	public static int reaMaksimum(int[] rida) {
		int seniSuurim=rida[0];
		for (int i = 0; i < rida.length; i++) {
			if(rida[i] > seniSuurim)
				seniSuurim=rida[i];
			
		}
		
		return seniSuurim;
		
	}
	public static int[] ridadeMaksimumid(int[][] maatriks){
		int[] maksimumid = new int[maatriks.length];
		for (int i = 0; i < maatriks.length; i++) {
			maksimumid[i] = reaMaksimum(maatriks[i]);	
		}
		return maksimumid;
		
		
	}
	public static void Tryki(int[] Numbrid){
	
		for (int i = 0; i < Numbrid.length; i++) {
			System.out.print(Numbrid[i] + " ");
		}
		
	}
	public static int miinimum(int[][] maatriks){
		int seniVäikseim = Integer.MAX_VALUE;
		for(int[] rida : maatriks){
			for(int number : rida){
				if(number < seniVäikseim){
					number = seniVäikseim;
				}
			}
		}
		return seniVäikseim;
 	}
		
	

}
