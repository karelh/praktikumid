package praktikum12;

public class KõrvalDiagonaaliSumma {
	
	public static void main(String[] args){
		int [][] neo = {
				{1, 1, 1, 1, 1},
				{2, 3, 4, 5, 6},
				{3, 4, 5, 6, 7},
				{4, 5, 6, 7, 8},
				{5, 6, 7, 8, 9},
 };
		System.out.println(kõrvaldiagonaaliSumma(neo));
	}

	public static int kõrvaldiagonaaliSumma(int[][] maatriks) {
		int summa = 0;
		for (int i = 0; i < maatriks.length; i++) {
			for (int j = 0; j < maatriks[i].length; j++) {
				if (i + j == maatriks.length - 1) {
					summa += maatriks[i][j];
				}

			}

		}
		return summa;
	}

}
