package praktikum12;

public class RidadeSumma {
	
	public static void main(String[] args){
		int [][] neo = {
				{1, 1, 1, 1, 1},
				{2, 3, 4, 5, 6},
				{3, 4, 5, 6, 7},
				{4, 5, 6, 7, 8},
				{5, 6, 7, 8, 9},
		};
		Tryki(ridadeSummad(neo));
	}
	public static int[] ridadeSummad(int[][] maatriks){
		int[] summad = new int[maatriks.length];
		for (int i = 0; i < maatriks.length; i++) {
			summad[i] = reaSumma(maatriks[i]);	
		}
		return summad;
		
		
	}
	public static int reaSumma(int[] rida){
		int summa=0;
		for(int i : rida){
			summa += i;
		}
		return summa;
	}
	public static void Tryki(int[] Numbrid){
		System.out.print("ridade summad: ");
		for (int i = 0; i < Numbrid.length; i++) {
			System.out.print(Numbrid[i] + " ");
		}
	}

}
