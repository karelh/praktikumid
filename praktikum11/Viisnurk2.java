package praktikum11;

import java.applet.Applet;
import java.awt.*;

public class Viisnurk2 extends Applet {
	private static final long serialVersionUID = 1L;

	@Override
    public void paint(Graphics g) {
		int w = getWidth();
		int h = getHeight();
        int x0 = w/2; // Keskpunkt
        int y0 = h/2;
        int raadius = 100; // Raadius
        int sammudeArv = 5; // Nurkade arv
        int tiirudeArv = 2; // "Tiirude" arv
        double t0 = Math.PI / 2; // Faas ehk kust alustada
        int x, y, eelmineX = 0, eelmineY = 0;
        double t;
        boolean esimene = true;
      

        // Kysime laiuse / ko~rguse

        // Katame tausta
        g.setColor(Color.white);
        g.fillRect(0, 0, w, h);

        // Joonistame
        g.setColor(Color.black);

        for (t = 0; t <= 2 * tiirudeArv * Math.PI; t = t + 2 * tiirudeArv * Math.PI / sammudeArv) {
            x = (int) (raadius * Math.cos(t + t0) + x0);
            y = (int) (raadius * Math.sin(t + t0) + y0);
            if (esimene) {
                esimene = false;
            }
            else {
                g.drawLine(x, y, eelmineX, eelmineY);
            }
            eelmineX = x;
            eelmineY = y;
        }
     
            g.setColor(Color.black);
            int keskkohtX = getWidth() / 2;
            int keskkohtY = getHeight() / 2;
            int kaadius = 100;
            
            for (double nurk = 0; nurk <= Math.PI * 2; nurk = nurk + 0.001) {
                int p = (int) (kaadius * Math.cos(nurk));
                int o = (int) (kaadius * Math.sin(nurk));
                g.fillRect(keskkohtX + p, keskkohtY + o, 1, 1);
            }
        
    }
}
