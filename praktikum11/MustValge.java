package praktikum11;

import java.applet.Applet;
import java.awt.*;

public class MustValge extends Applet {

	private static final long serialVersionUID = 1L;

	public void paint(Graphics g) {
		int w = getWidth();
		int h = getHeight();

		double v�rvimuutus = h;

		for (int i = 0; i < h; i++) {
			int v�rvikood = (int) (255 - i * (255 / v�rvimuutus));
			Color minuV�rv = new Color(v�rvikood, v�rvikood, v�rvikood);
			System.out.println(minuV�rv);
			g.setColor(minuV�rv);
			g.drawLine(0, i, w, i);
			
		}
	}
}