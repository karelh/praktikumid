package praktikum4;

public class TabelX {

	public static void main(String[] args) {
	
		int tabelisuurus = 15;
		for (int i = 0; i < tabelisuurus*2+3; i++) {
			System.out.print("-");	
		}
		System.out.println();
		for (int j = 0; j < tabelisuurus; j++) {
			System.out.print("| ");
			for (int i = 0; i < tabelisuurus; i++) {
				if(i==j || i+j==tabelisuurus-1)
				System.out.print("x ");
				else
				System.out.print("0 ");
			}
			System.out.print("| ");
			System.out.println();
		}
		for (int i = 0; i < tabelisuurus*2+3; i++) {
			System.out.print("-");	
		}
	}
}
