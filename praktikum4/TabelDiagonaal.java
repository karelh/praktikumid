package praktikum4;

public class TabelDiagonaal {

	public static void main(String[] args) {
		int tabelisuurus = 5;
		for (int j = 0; j < tabelisuurus; j++) {
			for (int i = 0; i < tabelisuurus; i++) {
				if(i==j)
				System.out.print("1 ");
				else
				System.out.print("0 ");
			System.out.print("(j=" + j +" i=" +i+")");
			}
			System.out.println();
		}
	}
}
